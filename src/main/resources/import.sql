-- Inicializar dados
insert into usuario(email,senha,ativo) values('admin@admin.com','$2a$10$h3UeZKKsR.ZMp5ucih4aR.GwMsKgfSx4w41.1vt4KAWTzYl4FiuQS',true);
insert into autorizacao(email,role,ativo) values('admin@admin.com','ROLE_ADMIN',true);
insert into perfil (nome,email) values ('Admin','admin@admin.com');
