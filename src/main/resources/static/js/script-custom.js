/**
 * Adicionar class active nos menus
 */
var pathname = location.pathname;
var menuItens = jQuery("nav>div>ul.nav>li.nav-item>a.nav-link");
menuItens.removeClass("active");
menuItens.each(function(i, d) {
	var elem = jQuery(d);
	var path = elem.attr("href");
	
	const regex_ = /^\//gm;
	const regex = /\/.*/gm;
	
	pathname = pathname.replace(regex_,"").replace(regex, "");
	path = path.replace(regex_,"");
	
	if (pathname == path) {
		elem.addClass("active");
		return false;
	}
})

/**
 * Iniciar data tables
 */
jQuery('table').DataTable({
	language: {
		url: "dataTables/1.10.18/i18n/Portuguese-Brasil.json"
	}
});



/**
 Modificar botões dos menus de expansão 
*/
jQuery("div.collapse").on("hidden.bs.collapse", function(){
	var link_attr = jQuery(this).prev("h6.sidebar-heading").children("a.d-flex").children("i");
	link_attr.attr("class", "fas fa-plus-circle");
})

jQuery("div.collapse").on("shown.bs.collapse", function(){
	var link_attr = jQuery(this).prev("h6.sidebar-heading").children("a.d-flex").children("i");
	link_attr.attr("class", "fas fa-minus-circle");
})


/**/
jQuery('[data-toggle="tooltip"]').tooltip();

