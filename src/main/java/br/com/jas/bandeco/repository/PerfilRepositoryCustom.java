package br.com.jas.bandeco.repository;

import br.com.jas.bandeco.model.Perfil;

public interface PerfilRepositoryCustom {

	public Integer editar(Perfil perfil);
	
}
