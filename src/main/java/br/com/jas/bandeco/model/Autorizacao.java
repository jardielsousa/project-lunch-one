package br.com.jas.bandeco.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.jas.bandeco.enumeration.TipoRole;

@Entity
public class Autorizacao implements Serializable {

	private static final long serialVersionUID = 4236685072096226553L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="email")
	private Usuario usuario;
	
	@Enumerated(EnumType.STRING)
	private TipoRole role;
	
	private Boolean ativo;
	
	public Autorizacao() {
		this.role = TipoRole.ROLE_USER;
		this.ativo = Boolean.TRUE;
	}
	
	public Autorizacao(Usuario usuario) {
		this();
		this.usuario = usuario;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoRole getRole() {
		return role;
	}

	public void setRole(TipoRole role) {
		this.role = role;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public String toString() {
		return this.role.getDescricao();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		System.out.println("Autorizacao.equals()");
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Autorizacao)) {
			return false;
		}
		Autorizacao other = (Autorizacao) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (role != other.role) {
			return false;
		}
		return true;
	}
	
}
