package br.com.jas.bandeco.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import br.com.jas.bandeco.model.Perfil;
import br.com.jas.bandeco.service.PerfilService;
import br.com.jas.bandeco.service.UsuarioService;

@Configuration
@EnableWebSecurity
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private PerfilService perfilService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/bootstrap/**","/css/**","/js/**","/img/**","favicon.ico").permitAll()
				.anyRequest().authenticated()
		.and()
			.formLogin()
				.loginPage("/login")
				.usernameParameter("email")
				.passwordParameter("senha")
//				.defaultSuccessUrl("/perfil-usuario/session")
				.successHandler(successHandler())
				.permitAll()
		.and()
			.logout()
			.permitAll()
		;
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		String queryUsuario = "SELECT u.email,u.senha,u.ativo FROM usuario u WHERE u.email=?";
		String queryAutorizacao = "SELECT a.email, a.role FROM autorizacao a WHERE a.ativo=true AND a.email=?";
		
		auth
			.jdbcAuthentication()
				.dataSource(dataSource)
				.passwordEncoder(usuarioService.getPasswordEncoder())
				.usersByUsernameQuery(queryUsuario)
				.authoritiesByUsernameQuery(queryAutorizacao)
		;
	}
	
	/**
	 * 
	 * @return
	 */
	public AuthenticationSuccessHandler successHandler() {
		AuthenticationSuccessHandler successHandler = new AuthenticationSuccessHandler() {
			
			@Override
			public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
					Authentication authentication) throws IOException, ServletException {
				String name = authentication.getName();
				Perfil perfil = perfilService.findByEmail(name);
				
				httpSession.setAttribute("perfilSession", perfil);
				
				response.sendRedirect("/");
			}
		};
		
		return successHandler;
	}
	
}
