package br.com.jas.bandeco.representacao;

import java.io.Serializable;

import br.com.jas.bandeco.model.Perfil;

public class FormAlterarSenha implements Serializable {

	private static final long serialVersionUID = 1563036730059892249L;
	
	private Long perfilId;
	
	private String email;
	
	private String senhaAtual;
	
	private String novaSenha;
	
	private String confirmarNovaSenha;

	public FormAlterarSenha() { }
	
	public FormAlterarSenha(Perfil perfil) {
		this.perfilId = perfil.getId();
		this.email = perfil.getUsuario().getEmail();
	}

	public Long getPerfilId() {
		return perfilId;
	}

	public void setPerfilId(Long perfilId) {
		this.perfilId = perfilId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public String getConfirmarNovaSenha() {
		return confirmarNovaSenha;
	}

	public void setConfirmarNovaSenha(String confirmarNovaSenha) {
		this.confirmarNovaSenha = confirmarNovaSenha;
	}

	@Override
	public String toString() {
		return "FormAlterarSenha [email=" + email + ", senhaAtual=" + senhaAtual + ", novaSenha=" + novaSenha
				+ ", confirmarNovaSenha=" + confirmarNovaSenha + "]";
	}
	
	
}
