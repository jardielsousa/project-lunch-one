package br.com.jas.bandeco.enumeration;

public enum TipoRole {
	ROLE_ADMIN("Administrador"),
	ROLE_USER("Usuário")
	;

	private String descricao;
	
	private TipoRole(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
