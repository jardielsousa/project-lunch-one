package br.com.jas.bandeco.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.jas.bandeco.enumeration.TipoRole;
import br.com.jas.bandeco.model.Autorizacao;
import br.com.jas.bandeco.model.Usuario;
import br.com.jas.bandeco.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository repository;
	
	private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}
	
	public String passwordEncoder(CharSequence rawPassword) {
		return this.passwordEncoder.encode(rawPassword);
	}
	
	public boolean isIguais(CharSequence rawPassword, String encodedPassword) {
		return this.passwordEncoder.matches(rawPassword, encodedPassword);
	}

	public List<Usuario> buscarTodos() {
		return this.repository.findAll();
	}
	
	public boolean existsById(String email) {
		return this.repository.existsById(email);
	}
	
	public Integer atualizarSenha(String novaSenha, String email) {
		String encode = passwordEncoder.encode(novaSenha);
		
		return this.repository.atualizarSenha(encode, email);
	}
	
	public Boolean isMesmaSenha(String outraSenha, String email) {
		String senha = this.repository.senha(email);
		boolean iguais = this.isIguais(outraSenha, senha);
		
		return iguais;
	}
	
	public void encodeSenha(Usuario usuario) {
		String senha = usuario.getSenha();
		if (senha.trim().length() > 0) {
			String encode = passwordEncoder(senha);
			usuario.setSenha(encode);
		}
	}
	
	public void addRole(Usuario usuario, TipoRole role) {
		Autorizacao a = new Autorizacao(usuario);
		a.setRole(role);
		
		usuario.getAutorizacoes().add(a);
	}

}
