package br.com.jas.bandeco.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jas.bandeco.model.Autorizacao;
import br.com.jas.bandeco.repository.AutorizacaoRepository;

@Service
public class AutorizacaoService {

	@Autowired
	private AutorizacaoRepository repository;
	
	public Autorizacao findById(Long id) {
		return this.repository.findById(id).orElse(null);
	}
	
	public Integer desabilitar(Autorizacao a) {
		return this.repository.desabilitar(a);
	}
	
	public Integer habilitar(Autorizacao a) {
		return this.repository.habilitar(a);
	}

	public Autorizacao save(Autorizacao autorizacao) {
		return this.repository.save(autorizacao);
	}

	public void editar(Autorizacao autorizacao) {
		this.repository.editarRole(autorizacao.getRole(), autorizacao);
	}
}
