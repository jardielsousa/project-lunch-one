package br.com.jas.bandeco.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("pedido")
public class PedidoController {

	@GetMapping
	public ModelAndView lista() {
		return new ModelAndView("/pedido/lista");
	}
	
}
