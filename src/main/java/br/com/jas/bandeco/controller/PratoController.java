package br.com.jas.bandeco.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jas.bandeco.model.Prato;
import br.com.jas.bandeco.service.PratoService;

@Controller
@RequestMapping("prato")
public class PratoController {

	private static final String PRATO_FORM_NOVO_ATUALIZAR_PRATO = "prato/form_novo_atualizar_prato";

	@Autowired
	private PratoService service;
	
	@GetMapping
	public ModelAndView lista() {
		List<Prato> pratos = this.service.findAll();
		ModelAndView mv = new ModelAndView("prato/lista_prato");
		mv.addObject("pratos",pratos);
		
		return mv;
	}
	
	@GetMapping("novo")
	public ModelAndView novo(Prato prato) {
		return new ModelAndView(PRATO_FORM_NOVO_ATUALIZAR_PRATO, "prato", prato);
	}
	
	@PostMapping("novo")
	public String criar(@Valid Prato prato, BindingResult bindingResult) {
		System.out.println("PratoController.criar()");
		
		if (bindingResult.hasErrors()) {
			return PRATO_FORM_NOVO_ATUALIZAR_PRATO;
		}
		
		this.service.save(prato);
		
		return "redirect:/prato";
	}
	
	@GetMapping("{pratoid}/editar")
	public ModelAndView editar(@PathVariable Long pratoid) {
		Prato prato = this.service.findById(pratoid);
		return new ModelAndView(PRATO_FORM_NOVO_ATUALIZAR_PRATO, "prato", prato);
	}
	
}
