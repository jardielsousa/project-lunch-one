package br.com.jas.bandeco.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jas.bandeco.enumeration.TipoRole;
import br.com.jas.bandeco.model.Usuario;
import br.com.jas.bandeco.service.UsuarioService;

@Controller
@RequestMapping("usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@ModelAttribute("roles")
	public TipoRole[] roles() {
		return TipoRole.values();
	}
	
	@GetMapping
	public ModelAndView lista() {
		List<Usuario> list = this.usuarioService.buscarTodos();
		ModelAndView mv = new ModelAndView("usuario/lista_usuario");
		mv.addObject("usuarios", list);
		
		return mv;
	}
	
}
