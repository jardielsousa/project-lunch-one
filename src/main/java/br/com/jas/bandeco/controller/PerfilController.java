package br.com.jas.bandeco.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jas.bandeco.enumeration.TipoRole;
import br.com.jas.bandeco.model.Autorizacao;
import br.com.jas.bandeco.model.Perfil;
import br.com.jas.bandeco.representacao.FormAlterarSenha;
import br.com.jas.bandeco.service.PerfilService;

@Controller
@RequestMapping("perfil")
public class PerfilController {
	
	private static final String USUARIO_ALTERAR_SENHA = "usuario/alterar_senha";

	private static final String PERFIL_FORM_CRIAR_EDITAR = "perfil/form_criar_atualizar_perfil";

	private static final String AUTORIZACAO_FORM_CRIAR_ATUALIZAR = "autorizacao/form_criar_atualizar_autorizacao";
	
	@Autowired
	private PerfilService service;

	@ModelAttribute("roles")
	public TipoRole[] roles() {
		return TipoRole.values();
	}
	
	@GetMapping
	@Secured("ROLE_ADMIN")
	public ModelAndView lista() {
		List<Perfil> findAll = this.service.findAll();
		ModelAndView mv = new ModelAndView("perfil/lista_perfil", "perfis", findAll);
		
		return mv;
	}
	
	@GetMapping("novo")
	public ModelAndView formCriarAtualizar(Perfil perfil) {
		ModelAndView mv = new ModelAndView(PERFIL_FORM_CRIAR_EDITAR);
		mv.addObject(perfil);
		
		return mv;
	}
	
	@GetMapping("{perfilid}")
	public ModelAndView exibir(@PathVariable("perfilid") Long id) {
		Perfil perfil = this.service.findById(id);
		
		return new ModelAndView("perfil/exibir_perfil", "perfil", perfil);
	}
	
	@GetMapping("{perfilid}/editar")
	public ModelAndView editar(@PathVariable("perfilid") Long id) {
		Perfil perfil = this.service.findById(id);
		ModelAndView mv = new ModelAndView(PERFIL_FORM_CRIAR_EDITAR);		
		mv.addObject(perfil);
		
		return mv;
	}
	
	@GetMapping("{perfilid}/autorizacao/novo")
	public ModelAndView adicionarAutorizacao(@PathVariable("perfilid") Long id) {
		Perfil perfil = this.service.findById(id);
		Autorizacao a = new Autorizacao(perfil.getUsuario());
		ModelAndView mv = new ModelAndView(AUTORIZACAO_FORM_CRIAR_ATUALIZAR);		
		mv.addObject(a);
		
		return mv;
	}

	@PostMapping("{perfilid}/autorizacao/novo")
	public String criarAutorizacao(@PathVariable("perfilid") Long id, Autorizacao autorizacao, BindingResult bindingResult) {
		Perfil perfil = this.service.findById(id);
		Set<Autorizacao> autorizacoes = perfil.getUsuario().getAutorizacoes();
		boolean anyMatch = autorizacoes.stream().anyMatch(a -> a.getRole().equals(autorizacao.getRole()));
		if (anyMatch) {
			bindingResult.rejectValue("role", "role.registered");
			return AUTORIZACAO_FORM_CRIAR_ATUALIZAR;
		}
		autorizacoes.add(autorizacao);
		this.service.save(perfil);
		
		return "redirect:/perfil/{perfilid}";
	}

	@PostMapping
	public String criar(Perfil perfil, BindingResult bindingResult) {
		this.service.validate(perfil, bindingResult);
		if (bindingResult.hasErrors()) {
			return PERFIL_FORM_CRIAR_EDITAR;
		}
		
		this.service.encodeSenha(perfil.getUsuario());
		perfil = this.service.save(perfil);
		
		return "redirect:/perfil/"+perfil.getId();
	}
	
	@PutMapping
	public String editar(Perfil perfil, BindingResult bindingResult) {
		this.service.validate(perfil, bindingResult);
		if (bindingResult.hasErrors()) {
			return PERFIL_FORM_CRIAR_EDITAR;
		}
		
		this.service.editar(perfil);
		
		return "redirect:/perfil/"+perfil.getId();
	}
	
	@GetMapping("{perfilId}/alterar-senha")
	public ModelAndView formAlterarSenha(@PathVariable Long perfilId) {
		Perfil perfil = this.service.findById(perfilId);
		FormAlterarSenha formAlterarSenha = new FormAlterarSenha(perfil);
		
		return new ModelAndView(USUARIO_ALTERAR_SENHA, "formAlterarSenha", formAlterarSenha);
	}
	
	@PutMapping("{perfilid}/alterar-senha")
	public String alterarSenha(FormAlterarSenha formAlterarSenha, BindingResult bindingResult) {
		this.service.validateFormAlterarSenha(formAlterarSenha, bindingResult);
		if (bindingResult.hasErrors()) {
			return USUARIO_ALTERAR_SENHA;
		}
		
		this.service.atualizarSenha(formAlterarSenha.getNovaSenha(), formAlterarSenha.getEmail());
		
		return "redirect:/perfil/{perfilid}";
	}
	
}
