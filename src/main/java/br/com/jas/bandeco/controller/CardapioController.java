package br.com.jas.bandeco.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("cardapio")
public class CardapioController {

	@GetMapping
	public ModelAndView lista() {
		ModelAndView mv = new ModelAndView("/cardapio/lista");
		
		return mv;
	}
	
}
