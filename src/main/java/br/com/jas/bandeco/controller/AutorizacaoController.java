package br.com.jas.bandeco.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jas.bandeco.enumeration.TipoRole;
import br.com.jas.bandeco.model.Autorizacao;
import br.com.jas.bandeco.model.Perfil;
import br.com.jas.bandeco.service.AutorizacaoService;
import br.com.jas.bandeco.service.PerfilService;

@Controller
@RequestMapping("perfil/{perfilid}/autorizacao")
public class AutorizacaoController {

	private static final String AUTORIZACAO_FORM_CRIAR_ATUALIZAR = "autorizacao/form_criar_atualizar_autorizacao";
	
	private static final String REDIRECT_PERFIL_PERFIL_ID = "redirect:/perfil/{perfilid}";
	
	@Autowired
	private AutorizacaoService service;
	
	@Autowired
	private PerfilService perfilService;
	
	@ModelAttribute("roles")
	public TipoRole[] roles() {
		return TipoRole.values();
	}
	
	@ModelAttribute("perfilid")
	public Long perfilId(@PathVariable("perfilid") Long perfilId) {
		return perfilId;
	}
	
	@GetMapping("{autorizacaoid}/desativar")
	public String desativar(@PathVariable("autorizacaoid") Long autorizacaoid) {
		Autorizacao a = this.service.findById(autorizacaoid);
		this.service.desabilitar(a);
		
		return REDIRECT_PERFIL_PERFIL_ID;
	}

	@GetMapping("{autorizacaoid}/ativar")
	public String ativar(@PathVariable("autorizacaoid") Long autorizacaoid) {
		Autorizacao a = this.service.findById(autorizacaoid);
		this.service.habilitar(a);
		
		return REDIRECT_PERFIL_PERFIL_ID;
	}

	@GetMapping("{autorizacaoid}/editar")
	public ModelAndView editarForm(@PathVariable("autorizacaoid") Long autorizacaoid) {
		Autorizacao a = this.service.findById(autorizacaoid);
		ModelAndView mv = new ModelAndView(AUTORIZACAO_FORM_CRIAR_ATUALIZAR);		
		mv.addObject(a);

		return mv;
	}

	@PostMapping("{autorizacaoid}/editar")
	public String editar(@PathVariable("perfilid") Long perfilId, Autorizacao autorizacao, BindingResult bindingResult) {
		Perfil perfil = this.perfilService.findById(perfilId);
		Set<Autorizacao> autorizacoes = perfil.getUsuario().getAutorizacoes();
		boolean anyMatch = autorizacoes.stream().anyMatch(a -> a.getRole().equals(autorizacao.getRole()));
		if (anyMatch) {
			bindingResult.rejectValue("role", "role.registered");
			return AUTORIZACAO_FORM_CRIAR_ATUALIZAR;
		}
		this.service.editar(autorizacao);
		
		return REDIRECT_PERFIL_PERFIL_ID;
	}

}
