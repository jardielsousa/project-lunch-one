package br.com.jas.bandeco.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.jas.bandeco.model.Perfil;
import br.com.jas.bandeco.representacao.FormAlterarSenha;
import br.com.jas.bandeco.service.PerfilService;

@Controller
@RequestMapping("perfil-usuario")
public class PerfilUsuarioController {

	private static final String PERFIL_SESSION = "perfilSession";

	private static final String USUARIO_ALTERAR_SENHA = "usuario/alterar_senha";

	@Autowired
	private PerfilService service;
	
	@Autowired
	private HttpSession httpSession;
	
	@GetMapping
	public ModelAndView mostrarPerfil() {
		Perfil perfil = (Perfil) httpSession.getAttribute(PERFIL_SESSION);
		
		return new ModelAndView("perfil/exibir-perfil-usuario", "perfil", perfil);
	}
	
	@GetMapping("alterar-senha")
	public ModelAndView formAtualizarSenha() {
		Perfil perfil = (Perfil) httpSession.getAttribute(PERFIL_SESSION);
		FormAlterarSenha formAlterarSenha = new FormAlterarSenha(perfil);
		
		return new ModelAndView(USUARIO_ALTERAR_SENHA, "formAlterarSenha", formAlterarSenha);
	}
	
	@PutMapping("alterar-senha")
	public String atualizarSenha(FormAlterarSenha formAlterarSenha, BindingResult bindingResult) {
		this.service.validateFormAlterarSenha(formAlterarSenha, bindingResult);
		if (bindingResult.hasErrors()) {
			return USUARIO_ALTERAR_SENHA;
		}
		
		this.service.atualizarSenha(formAlterarSenha.getNovaSenha(), formAlterarSenha.getEmail());
		
		return "redirect:/perfil-usuario";
	}
	
}
